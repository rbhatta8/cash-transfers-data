# cash-transfers-data

Repository for cash transfers data, a semi-synthetic version inspired by the study by [Baird et al](https://www.jstor.org/stable/pdf/41337177.pdf?casa_token=Q7qrxDMc5ucAAAAA:gr5TxpjVCrsGXoyhq3XAlDvFMJxS_i8fH4_jkTBVlfwxN4SzSj8Bg-s5UyrH8PRjSy26VVbZnOlbLDHxeZgLrjBzAVrhNncYgcIKo9VfxGbo0ZTHaVA)